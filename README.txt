Requirements to run the project:
PHP >= 7.1.3
OpenSSL PHP Extension
PDO PHP Extension
Mbstring PHP Extension
Tokenizer PHP Extension
XML PHP Extension
Ctype PHP Extension
JSON PHP Extension
BCMath PHP Extension

How to run the project:
1) Put the folder in wamp/www or xampp/htdoc
2) Go to localhost/phpmyadmin, create database 'employee_expense_tracker' and import the sql file placed in employee-expense-traker/SQL
2) Open command prompt and go to the folder for eg: cd c:/wamp/www/employee-expense-traker
3) Then run this artisan command: php artisan serve it will give you a URL on which project will run.
4) I have already imported the psv file given by you.
5) Here are the some sample logins:

Admin: 
username: admin@mailinator.com
Password: admin@123456

User
username: kalyani@homepageit.net
Password: homepageit@123


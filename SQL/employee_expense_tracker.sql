-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 06, 2019 at 10:40 AM
-- Server version: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `employee_expense_tracker`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`,`name`,`created_at`,`updated_at`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Travel', '2019-05-04 02:42:50', '2019-05-04 02:42:50'),
(2, 'Meals and Entertainment', '2019-05-04 02:45:13', '2019-05-04 02:45:13'),
(3, 'Computer - Hardware', '2019-05-04 02:45:13', '2019-05-04 02:45:13'),
(4, 'Computer - Software', '2019-05-04 02:45:13', '2019-05-04 02:45:13'),
(5, 'Office Supplies', '2019-05-04 02:47:28', '2019-05-04 02:47:28'),
(7, 'Telecommute', '2019-05-04 05:50:56', '2019-05-04 05:50:56'),
(8, 'Employee Welfare', '2019-05-04 06:01:10', '2019-05-04 06:01:10');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` decimal(10,0) NOT NULL,
  `type` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`,`firstname`,`lastname`,`email`,`phone`,`type`,`status`,`password`,`created_at`,`updated_at`),
  KEY `USERTYPE_idx` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `phone`, `type`, `status`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Kalyani', 'Sehgal', 'kalyani@homepageit.net', '9977340080', 2, 1, '$2y$10$ypzmdTk9iZ7xlLZR0b79VeOjto7/oZzi5wzl95OqSBpsqFVte.15e', '2019-05-03 03:26:04', '2019-05-03 03:26:04'),
(2, 'Manita', 'Kriplani', 'manita.kriplani@homepageit.net', '7539518524', 2, 1, '$2y$10$L98ddILxsp.cDEm5O.MpyuHjyJAwJXxLp8NhgvuE9b/PpXPK3QkGe', '2019-05-03 03:32:51', '2019-05-03 03:32:51'),
(3, 'Joe', 'Miler', 'joe@mailinator.com', '2589637418', 2, 1, '$2y$10$pJ5SV7TPfAVPuCBoLs7aUeQ74wwKeYmBYe5SQk1J6t34BdUHmSVRO', '2019-05-03 03:33:36', '2019-05-03 03:33:36'),
(4, 'Admin', 'Account', 'admin@mailinator.com', '7539518524', 1, 1, '$2y$10$BPRVUypwaGwEd0AVoffHNOH9Gow9I3m5JQNe5TS.qMEIV1UWlrCz2', '2019-05-03 04:32:23', '2019-05-03 04:32:23');

-- --------------------------------------------------------

--
-- Table structure for table `user_expense`
--

DROP TABLE IF EXISTS `user_expense`;
CREATE TABLE IF NOT EXISTS `user_expense` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_name` varchar(255) DEFAULT NULL,
  `employee_address` text,
  `expense_date` timestamp NULL DEFAULT NULL,
  `expense_description` text,
  `category` int(11) DEFAULT NULL,
  `pre_tax_amount` float DEFAULT NULL,
  `tax_amount` float DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `EXPCATEGORY_idx` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_expense`
--

INSERT INTO `user_expense` (`id`, `employee_name`, `employee_address`, `expense_date`, `expense_description`, `category`, `pre_tax_amount`, `tax_amount`, `created_at`, `updated_at`) VALUES
(1, 'Steve Rogers', '783 Park Ave New York NY 10021', '2018-11-30 18:30:00', 'Taxi ride', 1, 350, 31.06, '2019-05-04 02:45:13', '2019-05-04 02:45:13'),
(2, 'Tony Stark', '1 Infinite Loop Cupertino CA 95014', '2018-12-14 18:30:00', 'Team lunch', 2, 235, 17.63, '2019-05-04 02:45:13', '2019-05-04 02:45:13'),
(3, 'Bruce Banner', '1 Infinite Loop Cupertino CA 95014', '2018-12-30 18:30:00', 'HP Laptop', 3, 999, 74.93, '2019-05-04 02:45:13', '2019-05-04 02:45:13'),
(4, 'Nick Fury', '1 Infinite Loop Cupertino CA 95014', '2018-12-13 18:30:00', 'Microsoft Office', 4, 899, 67.43, '2019-05-04 02:45:13', '2019-05-04 02:45:13'),
(5, 'Natasha Romanoff', '1600 Amphitheatre Parkway Mountain View CA 94043', '2018-12-05 18:30:00', 'iCloud Subscription', 4, 50, 3.75, '2019-05-04 02:45:13', '2019-05-04 02:45:13'),
(6, 'Stephen Strange', '1600 Amphitheatre Parkway Mountain View CA 94043', '2018-12-08 18:30:00', 'iCloud Subscription', 4, 50, 3.75, '2019-05-04 02:45:13', '2019-05-04 02:45:13'),
(7, 'Carol Danvers', '1600 Amphitheatre Parkway Mountain View CA 94043', '2018-11-09 18:30:00', 'Coffee with Steve', 2, 300, 22.5, '2019-05-04 02:45:13', '2019-05-04 02:45:13'),
(8, 'Stephen Strange', '1600 Amphitheatre Parkway Mountain View CA 94043', '2018-11-11 18:30:00', 'Taxi ride', 1, 230, 17.25, '2019-05-04 02:45:13', '2019-05-04 02:45:13'),
(9, 'Steve Rogers', '783 Park Ave New York NY 10021', '2018-11-19 18:30:00', 'Client dinner', 2, 200, 15, '2019-05-04 02:45:13', '2019-05-04 02:45:13'),
(10, 'Carol Danvers', '1600 Amphitheatre Parkway Mountain View CA 94043', '2018-10-03 18:30:00', 'Flight to Miami', 1, 200, 15, '2019-05-04 02:45:13', '2019-05-04 02:45:13'),
(11, 'Steve Rogers', '783 Park Ave New York NY 10021', '1970-01-18 15:17:02', 'Taxi ride', 1, 350, 31.06, '2019-05-04 02:47:28', '2019-05-04 02:47:28'),
(12, 'Tony Stark', '1 Infinite Loop Cupertino CA 95014', '1970-01-18 15:37:12', 'Team lunch', 2, 235, 17.63, '2019-05-04 02:47:28', '2019-05-04 02:47:28'),
(13, 'Bruce Banner', '1 Infinite Loop Cupertino CA 95014', '1970-01-18 16:00:14', 'HP Laptop', 3, 999, 74.93, '2019-05-04 02:47:28', '2019-05-04 02:47:28'),
(14, 'Nick Fury', '1 Infinite Loop Cupertino CA 95014', '1970-01-18 15:35:45', 'Microsoft Office', 4, 899, 67.43, '2019-05-04 02:47:28', '2019-05-04 02:47:28'),
(15, 'Natasha Romanoff', '1600 Amphitheatre Parkway Mountain View CA 94043', '1970-01-18 15:24:14', 'iCloud Subscription', 4, 50, 3.75, '2019-05-04 02:47:28', '2019-05-04 02:47:28'),
(16, 'Stephen Strange', '1600 Amphitheatre Parkway Mountain View CA 94043', '1970-01-18 15:28:33', 'iCloud Subscription', 4, 50, 3.75, '2019-05-04 02:47:28', '2019-05-04 02:47:28'),
(17, 'Carol Danvers', '1600 Amphitheatre Parkway Mountain View CA 94043', '1970-01-18 14:46:48', 'Coffee with Steve', 2, 300, 22.5, '2019-05-04 02:47:28', '2019-05-04 02:47:28'),
(18, 'Stephen Strange', '1600 Amphitheatre Parkway Mountain View CA 94043', '1970-01-18 14:49:40', 'Taxi ride', 1, 230, 17.25, '2019-05-04 02:47:28', '2019-05-04 02:47:28'),
(19, 'Steve Rogers', '783 Park Ave New York NY 10021', '1970-01-18 15:01:12', 'Client dinner', 2, 200, 15, '2019-05-04 02:47:28', '2019-05-04 02:47:28'),
(20, 'Carol Danvers', '1600 Amphitheatre Parkway Mountain View CA 94043', '1970-01-18 13:53:31', 'Flight to Miami', 1, 200, 15, '2019-05-04 02:47:28', '2019-05-04 02:47:28'),
(21, 'Steve Rogers', '783 Park Ave New York NY 10021', '1970-02-11 07:52:04', 'Macbook Air', 3, 1, 999, '2019-05-04 02:47:28', '2019-05-04 02:47:28'),
(22, 'Tony Stark', '1 Infinite Loop Cupertino CA 95014', '1970-01-18 15:28:33', 'Dropbox Subscription', 4, 15, 1.13, '2019-05-04 02:47:28', '2019-05-04 02:47:28'),
(23, 'Nick Fury', '1 Infinite Loop Cupertino CA 95014', '1970-01-18 13:30:28', 'Taxi ride', 1, 200, 15, '2019-05-04 02:47:28', '2019-05-04 02:47:28'),
(24, 'Stephen Strange', '1600 Amphitheatre Parkway Mountain View CA 94043', '1970-01-18 13:47:45', 'Paper', 5, 200, 15, '2019-05-04 02:47:28', '2019-05-04 02:47:28'),
(25, 'Stephen Strange', '1600 Amphitheatre Parkway Mountain View CA 94043', '1970-01-18 15:58:48', 'Dinner with potential acquisition', 2, 200, 15, '2019-05-04 02:47:28', '2019-05-04 02:47:28'),
(26, 'Carol Danvers', '1600 Amphitheatre Parkway Mountain View CA 94043', '1970-01-18 16:08:52', 'iPhone', 3, 200, 15, '2019-05-04 02:47:28', '2019-05-04 02:47:28'),
(27, 'Tony Stark', '1 Infinite Loop Cupertino CA 95014', '1970-01-18 16:10:19', 'Airplane ticket to NY', 1, 200, 15, '2019-05-04 02:47:28', '2019-05-04 02:47:28'),
(28, 'Bruce Banner', '1 Infinite Loop Cupertino CA 95014', '1970-01-18 16:49:12', 'Starbucks coffee', 2, 12, 0.9, '2019-05-04 02:47:28', '2019-05-04 02:47:28'),
(29, 'Carol Danvers', '1600 Amphitheatre Parkway Mountain View CA 94043', '1970-01-18 17:10:48', 'Airplane ticket to NY', 1, 1500, 112.5, '2019-05-04 02:47:28', '2019-05-04 02:47:28'),
(30, 'Kalyani Sehgal', 'OR - 70 Indus Satellite Greens, Kelod Hala, Behind Panchwati Indore', '1970-01-18 18:54:28', 'Call with client', 7, 185.26, 11.02, '2019-05-04 05:50:56', '2019-05-04 05:50:56'),
(31, 'Kalyani Sehgal', 'Address 1, Address 2', '2019-04-30 18:30:00', 'Printer', 3, 100, 10, '2019-05-04 05:56:33', '2019-05-04 05:56:33'),
(32, 'Kalyani Sehgal', 'Another Address', '2019-05-02 18:30:00', 'Office supplies', 5, 250.65, 23.01, '2019-05-04 06:00:17', '2019-05-04 06:00:17'),
(33, 'Kalyani Sehgal', 'OR - 70 Indus Satellite Greens, Kelod Hala, Behind Panchwati Indore', '2019-04-23 18:30:00', 'Bday party celebration', 8, 200.65, 6.01, '2019-05-04 06:01:10', '2019-05-04 06:01:10');

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

DROP TABLE IF EXISTS `user_type`;
CREATE TABLE IF NOT EXISTS `user_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`,`name`,`created_at`,`updated_at`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-05-01 18:30:00', '2019-05-01 18:30:00'),
(2, 'user', '2019-05-01 18:30:00', '2019-05-02 12:30:00');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `USERTYPE` FOREIGN KEY (`type`) REFERENCES `user_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_expense`
--
ALTER TABLE `user_expense`
  ADD CONSTRAINT `EXPCATEGORY` FOREIGN KEY (`category`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
